kubernetes-on-etcd-one-click
============================

This stack will work with CloudCoreo to deploy a HA-Etcd cluster, a HA-Kubernetes-Master cluster and a HA-Kubernetes-Node cluster.


## Description
There has been a lot of talk about the Lithium Technologies' use of Kubernetes across cloud providers. While Lithium has chosen to keep the projects seperate, the promise has been a "one-click" deployment. This is that one-click deployment.

This consists of 3 seperate CloudCoreo stacks assembeled together to combine variables and ease the deployment process. Those stacks are:

[HA-Etcd cluster](http://hub.cloudcoreo.com/stack/etcd-cluster&#95;06252)
[HA-Kubernetes Master cluster](http://hub.cloudcoreo.com/stack/cloudcoreo-kubernetes-master-cluster&#95;39a3c)
[HA-Kubernetes Node Cluster](http://hub.cloudcoreo.com/stack/cloudcoreo-kubernetes-node-cluster&#95;67dd5)

Etcd will back the kubernetes master, which controls the nodes.

#### THINGS TO KEEP IN MIND

This entire cluster will default to ssh-disabled. Your only interaction will be through the API's provided by the services and the etcdctl and kubectl tools. If you need SSH enabled, you MUST supply an ***existing*** keypair for the region in which you are deploying. You do this by setting the variables:

  * `KUBE&#95;MASTER&#95;KEY`
  * `ETCD&#95;INSTANCE&#95;KEY&#95;NAME`
  * `KUBE&#95;NODE&#95;KEY`

Within the webapp, you supply via the "optional variables" dropdown. If you are using the `coreo solo run` method, specify `default: <your key name>`. Keep in mind yaml does not allow tab characters.

#### Prerequisite - VPC

You need to utilize a CloudCoreo managed VPC. If you already have a VPC. Here is a good one with a HA-NAT:
* [VPC with an HA NAT](http://hub.cloudcoreo.com/stack/cloudcoreo-vpc&#95;7aa05)

If you already have a VPC you want to launch in, Slack us and we'll show you how (or read the docs, but thats boring).

#### Other - VPN?
This setup is almost entirely contained in a private network space (aside from the DNS). This means that you must either have a bastion / jump box or use a VPN. We have one here if you need a VPN set up:
* [Self healing OpenVPN](http://hub.cloudcoreo.com/stack/servers-vpn&#95;63173)



## Hierarchy
![composite inheritance hierarchy](https://raw.githubusercontent.com/CloudCoreo/kubernetes-on-etcd-one-click/master/images/hierarchy.png "composite inheritance hierarchy")



## Required variables with no default

### `DNS_ZONE`:
  * description: the dns zone (eg. example.com)

### `ETCD_CLUSTER_AMI`:
  * description: the ami to launch for the etcd cluster - default is Amazon Linux AMI 2015.03 (HVM), SSD Volume Type

### `ETCD_DNS_ZONE`:
  * description: the zone in which the internal elb dns entry should be maintained

### `KUBE_CLUSTER_AMI`:
  * description: the ami to launch for the cluster - default is Amazon Linux AMI 2015.03 (HVM), SSD Volume Type


## Required variables with default

### `VPC_NAME`:
  * description: the cloudcoreo defined vpc to add this cluster to
  * default: test-vpc

### `VPC_CIDR`:
  * description: the cloudcoreo defined vpc to add this cluster to
  * default: 10.11.0.0/16

### `PRIVATE_ROUTE_NAME`:
  * description: the private subnet in which the cluster should be added
  * default: test-private-route

### `PRIVATE_SUBNET_NAME`:
  * description: the private subnet in which the cluster should be added
  * default: test-private-subnet

### `KUBE_MASTER_NAME`:
  * description: the name of the cluster - this will become your dns record too
  * default: kube-master

### `KUBE_MASTER_HEALTH_CHECK_GRACE_PERIOD`:
  * description: the time in seconds to allow for instance to boot before checking health
  * default: 600

### `KUBE_MASTER_UPGRADE_COOLDOWN`:
  * description: the time in seconds between rolling instances during an upgrade
  * default: 300

### `KUBE_NODE_NAME`:
  * description: the name of the cluster - this will become your dns record too
  * default: kube-node

### `TIMEZONE`:
  * description: the timezone the servers should come up in
  * default: America/Chicago


### `ETCD_PKG_VERSION`:
  * description: etcd version
  * default: 2.2.0

### `ETCD_CLUSTER_NAME`:
  * description: the name of the etcd cluster - this will become your dns record too
  * default: dev-etcd

### `ETCD_ELB_TRAFFIC_PORTS`:
  * description: ports that need to allow traffic into the ELB
  * default: 94b, 94c

### `ETCD_ELB_TRAFFIC_CIDRS`:
  * description: the cidrs to allow traffic from on the ELB itself
  * default: 10.0.0.0/8

### `ETCD_TCP_HEALTH_CHECK_PORT`:
  * description: the tcp port the ELB will check to verify ETCD is running
  * default: 2379

### `ETCD_CLUSTER_INSTANCE_TRAFFIC_PORTS`:
  * description: ports to allow traffic on directly to the instances
  * default: 94b, 94c, 16

### `ETCD_CLUSTER_INSTANCE_TRAFFIC_CIDRS`:
  * description: cidrs that are allowed to access the instances directly
  * default: 10.0.0.0/8

### `ETCD_INSTANCE_SIZE`:
  * description: the image size to launch
  * default: t2.small


### `ETCD_CLUSTER_SIZE_MIN`:
  * description: the minimum number of instances to launch
  * default: 3

### `ETCD_CLUSTER_SIZE_MAX`:
  * description: the maxmium number of instances to launch
  * default: 5

### `ETCD_INSTANCE_HEALTH_CHECK_GRACE_PERIOD`:
  * description: the time in seconds to allow for instance to boot before checking health
  * default: 600

### `ETCD_CLUSTER_UPGRADE_COOLDOWN`:
  * description: the time in seconds between rolling instances during an upgrade
  * default: 300

### `KUBE_VERSION`:
  * description: kubernetes version
  * default: 1.1.4

### `KUBE_MASTER_SERVICE_IP_CIDRS`:
  * description: kubernetes service cidrs
  * default: 10.1.1.0/24


### `KUBE_MASTER_ELB_TRAFFIC_PORTS`:
  * description: ports that need to allow traffic into the ELB
  * default: 1f90

### `KUBE_MASTER_ELB_TRAFFIC_CIDRS`:
  * description: the cidrs to allow traffic from on the ELB itself
  * default: 10.0.0.0/8

### `KUBE_MASTER_TCP_HEALTH_CHECK_PORT`:
  * description: a tcp port the ELB will check every so often - this defines health and ASG termination
  * default: 10251

### `KUBE_MASTER_INSTANCE_TRAFFIC_PORTS`:
  * description: ports to allow traffic on directly to the instances
  * default: 1..65535

### `KUBE_MASTER_INSTANCE_TRAFFIC_CIDRS`:
  * description: cidrs that are allowed to access the instances directly
  * default: 10.0.0.0/8

### `KUBE_MASTER_SIZE`:
  * description: the image size to launch
  * default: t2.small


### `KUBE_NODE_IP_CIDRS`:
  * description: Node IP CIDR block - NOTE - This MUST be different that the cidr specified for the kubernetes master service cidrs
  * default: 10.2.1.0/24


### `KUBE_NODE_IP_CIDRS_SUBDIVIDER`:
  * description: kubernetes service cidrs
  * default: 26

### `KUBE_NODE_TCP_HEALTH_CHECK_PORT`:
  * description: a tcp port the ELB will check every so often - this defines health and ASG termination
  * default: 10250

### `KUBE_NODE_INSTANCE_TRAFFIC_PORTS`:
  * description: ports to allow traffic on directly to the instances
  * default: 1..65535

### `KUBE_NODE_INSTANCE_TRAFFIC_CIDRS`:
  * description: cidrs that are allowed to access the instances directly
  * default: 10.0.0.0/8

### `KUBE_NODE_SIZE`:
  * description: the image size to launch
  * default: t2.small


### `KUBE_NODE_GROUP_SIZE_MIN`:
  * description: the minimum number of instances to launch
  * default: 3

### `KUBE_NODE_GROUP_SIZE_MAX`:
  * description: the maxmium number of instances to launch
  * default: 7

### `KUBE_NODE_HEALTH_CHECK_GRACE_PERIOD`:
  * description: the time in seconds to allow for instance to boot before checking health
  * default: 600

### `KUBE_NODE_UPGRADE_COOLDOWN`:
  * description: the time in seconds between rolling instances during an upgrade
  * default: 300


## Optional variables with default

### `ETCD_ELB_LISTENERS`:
  * description: The listeners to apply to the ELB
  * default: 
```
[      
  {      
      :elb_protocol => 'tcp',      
      :elb_port => 2379,      
      :to_protocol => 'tcp',      
      :to_port => 2379      
  },      
  {      
      :elb_protocol => 'tcp',      
      :elb_port => 2380,      
      :to_protocol => 'tcp',      
      :to_port => 2380      
  }      
]      

```

### `ETCD_WAIT_FOR_CLUSTER_MIN`:
  * description: true if the cluster should wait for all instances to be in a running state
  * default: true

### `KUBE_ALLOW_PRIVILEGED`:
  * description: allow privileged containers
  * default: true

### `KUBE_API_LOG_FILE`:
  * description: ha-nat log file
  * default: /var/log/kube-apiserver.log


### `KUBE_CONTROLLER_MANAGER_LOG_FILE`:
  * description: ha-nat log file
  * default: /var/log/kube-controller-manager.log


### `KUBE_SCHEDULER_LOG_FILE`:
  * description: ha-nat log file
  * default: /var/log/kube-scheduler.log


### `KUBE_PROXY_LOG_FILE`:
  * description: kube proxy log file
  * default: /var/log/kube-proxy.log


### `KUBE_MASTER_ELB_LISTENERS`:
  * description: The listeners to apply to the ELB
  * default: 
```
[      
  {      
      :elb_protocol => 'tcp',      
      :elb_port => 8080,      
      :to_protocol => 'tcp',      
      :to_port => 8080      
  }      
]      

```

### `WAIT_FOR_KUBE_MASTER_MIN`:
  * description: true if the cluster should wait for all instances to be in a running state
  * default: true

### `KUBE_KUBLET_LOG_FILE`:
  * description: kublet log file
  * default: /var/log/kube-kublet.log


### `KUBE_NODE_ELB_TRAFFIC_PORTS`:
  * description: leave this blank - we are using ELB for health checks only
  * default: 280a

### `KUBE_NODE_ELB_TRAFFIC_CIDRS`:
  * description: leave this blank - we are using ELB for health checks only
  * default: 10.0.0.0/8

### `KUBE_NODE_ELB_LISTENERS`:
  * description: ports to pass through the elb to the kubernetes node cluster instances
  * default: 
```
[      
  {      
      :elb_protocol => 'tcp',      
      :elb_port => 10250,      
      :to_protocol => 'tcp',      
      :to_port => 10250      
  }      
]      

```


## Optional variables with no default

### `VPC_SEARCH_TAGS`:
  * description: if you have more than one VPC with the same CIDR, and it is not under CloudCoreo control, we need a way to find it. Enter some unique tags that exist on the VPC you want us to find. ['env=production','Name=prod-vpc']

### `PRIVATE_ROUTE_SEARCH_TAGS`:
  * description: if you more than one route table or set of route tables, and it is not under CloudCoreo control, we need a way to find it. Enter some unique tags that exist on your route tables you want us to find. i.e. ['Name=my-private-routetable','env=dev']

### `PRIVATE_SUBNET_SEARCH_TAGS`:
  * description: Usually the private-routetable association is enough for us to find the subnets you need, but if you have more than one subnet, we may need a way to find them. unique tags is a great way. enter them there. i.e. ['Name=my-private-subnet']

### `DATADOG_KEY`:
  * description: If you have a datadog key, enter it here and we will install the agent

### `ETCD_INSTANCE_KEY_NAME`:
  * description: the ssh key to associate with the instance(s) - blank will disable ssh

### `KUBE_MASTER_KEY`:
  * description: the ssh key to associate with the instance(s) - blank will disable ssh

### `KUBE_NODE_KEY`:
  * description: the ssh key to associate with the instance(s) - blank will disable ssh

## Tags
1. Containers
1. High Availability
1. Multi-cluster
1. CoreOS
1. Google
1. Docker

## Categories
1. Servers



## Diagram
![kubernetes one click diagram](https://raw.githubusercontent.com/CloudCoreo/kubernetes-on-etcd-one-click/master/images/diagram.png "kubernetes in one click on etcd multi cluster diagram")


## Icon
![kubernetes one click icon](https://raw.githubusercontent.com/CloudCoreo/kubernetes-on-etcd-one-click/master/images/icon.png "kubernetes in one click on etcd multi cluster icon")


