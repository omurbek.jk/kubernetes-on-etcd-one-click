There has been a lot of talk about the Lithium Technologies' use of Kubernetes across cloud providers. While Lithium has chosen to keep the projects seperate, the promise has been a "one-click" deployment. This is that one-click deployment.

This consists of 3 seperate CloudCoreo stacks assembeled together to combine variables and ease the deployment process. Those stacks are:

[HA-Etcd cluster](http://hub.cloudcoreo.com/stack/etcd-cluster&#95;06252)
[HA-Kubernetes Master cluster](http://hub.cloudcoreo.com/stack/cloudcoreo-kubernetes-master-cluster&#95;39a3c)
[HA-Kubernetes Node Cluster](http://hub.cloudcoreo.com/stack/cloudcoreo-kubernetes-node-cluster&#95;67dd5)

Etcd will back the kubernetes master, which controls the nodes.

#### THINGS TO KEEP IN MIND

This entire cluster will default to ssh-disabled. Your only interaction will be through the API's provided by the services and the etcdctl and kubectl tools. If you need SSH enabled, you MUST supply an ***existing*** keypair for the region in which you are deploying. You do this by setting the variables:

  * `KUBE&#95;MASTER&#95;KEY`
  * `ETCD&#95;INSTANCE&#95;KEY&#95;NAME`
  * `KUBE&#95;NODE&#95;KEY`

Within the webapp, you supply via the "optional variables" dropdown. If you are using the `coreo solo run` method, specify `default: <your key name>`. Keep in mind yaml does not allow tab characters.

#### Prerequisite - VPC

You need to utilize a CloudCoreo managed VPC. If you already have a VPC. Here is a good one with a HA-NAT:
* [VPC with an HA NAT](http://hub.cloudcoreo.com/stack/cloudcoreo-vpc&#95;7aa05)

If you already have a VPC you want to launch in, Slack us and we'll show you how (or read the docs, but thats boring).

#### Other - VPN?
This setup is almost entirely contained in a private network space (aside from the DNS). This means that you must either have a bastion / jump box or use a VPN. We have one here if you need a VPN set up:
* [Self healing OpenVPN](http://hub.cloudcoreo.com/stack/servers-vpn&#95;63173)

